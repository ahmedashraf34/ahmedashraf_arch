

import 'package:ahmedashraf_arch/base/BaseView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'SampleViewModel.dart';

class SampleDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SampleViewModel>(
      onModelDispose: (model) {
        model.searchController.dispose();
      },
      builder: (ctx, model, child) {
        return CupertinoAlertDialog(
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('البحث',style: TextStyle(
              fontFamily: model.assets.fontFamily,fontSize: 17,fontWeight: FontWeight.bold,
            ),),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Material(
                          child: TextField(
                            controller: model.searchController,
                            textInputAction: TextInputAction.search,
                            onChanged: (value) {
                              model.onChangeSearchWord(value);
                              if (value == "") {
                                model.onClearSearchWord();
                                model.nav.goBack(results: model.searchWord);
                              }
                            },
                            onSubmitted: (value) {
                              if (model.searchWord != null) {
                                FocusScope.of(context);
                                model.nav.goBack(results: model.searchWord);
                              }
                            },
                            style: TextStyle(
                              color: model.colors.mainColor,
                            ),
                            textAlign: TextAlign.right,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: model.colors.mainColor2,
                              hintText: "اكتب بحثك هنا",
                              hintStyle: TextStyle(
                                color: model.colors.mainColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                              contentPadding: EdgeInsets.all(5),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: model.colors.mainColor,
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: model.colors.mainColor,
                                ),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: model.colors.mainColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    model.searchWord != null
                        ? GestureDetector(
                      onTap: () {
                        FocusScope.of(context);
                        model.onClearSearchWord();
                        model.nav.goBack(results: model.searchWord);
                      },
                      child: Icon(
                        Icons.close,
                        color: model.colors.mainColor,
                        size: 20,
                      ),
                    )
                        : Container(),
                  ],
                ),
              ),

            ],
          ),
          actions: [
            FlatButton(child: Text('بحث'),onPressed: (){
              if (model.searchWord != null) {
                FocusScope.of(context);
                model.nav.goBack(results: model.searchWord);
              }
              },)
          ],
        );
      },
    );
  }
}
