class SampleModel {
  String key;
  String value;
  DataBean data;

  SampleModel({this.key, this.value, this.data});

  SampleModel.fromJson(Map<String, dynamic> json) {
    this.key = json['key'];
    this.value = json['value'];
    this.data = json['data'] != null ? DataBean.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['value'] = this.value;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

}

class DataBean {
  List<CategoriesListBean> categories;
  List<OffersListBean> offers;
  List<ShopsListBean> shops;
  List<SliderListBean> slider;

  DataBean({this.categories, this.offers, this.shops, this.slider});

  DataBean.fromJson(Map<String, dynamic> json) {
    this.categories = (json['categories'] as List)!=null?(json['categories'] as List).map((i) => CategoriesListBean.fromJson(i)).toList():null;
    this.offers = (json['offers'] as List)!=null?(json['offers'] as List).map((i) => OffersListBean.fromJson(i)).toList():null;
    this.shops = (json['shops'] as List)!=null?(json['shops'] as List).map((i) => ShopsListBean.fromJson(i)).toList():null;
    this.slider = (json['slider'] as List)!=null?(json['slider'] as List).map((i) => SliderListBean.fromJson(i)).toList():null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categories'] = this.categories != null?this.categories.map((i) => i.toJson()).toList():null;
    data['offers'] = this.offers != null?this.offers.map((i) => i.toJson()).toList():null;
    data['shops'] = this.shops != null?this.shops.map((i) => i.toJson()).toList():null;
    data['slider'] = this.slider != null?this.slider.map((i) => i.toJson()).toList():null;
    return data;
  }
}

class CategoriesListBean {
  String logo;
  String name;
  int id;

  CategoriesListBean({this.logo, this.name, this.id});

  CategoriesListBean.fromJson(Map<String, dynamic> json) {
    this.logo = json['logo'];
    this.name = json['name'];
    this.id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class OffersListBean {
  String shop;
  String image;
  String name;
  String currency;
  String desc;
  String price;
  String discount;
  String fav;
  dynamic percentage;
  int id;
  int days;


  OffersListBean({this.currency,this.days,this.shop, this.image, this.name, this.desc, this.price, this.discount, this.fav, this.percentage, this.id});

  OffersListBean.fromJson(Map<String, dynamic> json) {
    this.shop = json['shop'];
    this.image = json['image'];
    this.name = json['name'];
    this.desc = json['desc'];
    this.price = json['price'];
    this.discount = json['discount'];
    this.fav = json['fav'];
    this.percentage = json['percentage'];
    this.id = json['id'];
    this.days = json['days'];
    this.currency = json['currency'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shop'] = this.shop;
    data['image'] = this.image;
    data['name'] = this.name;
    data['desc'] = this.desc;
    data['price'] = this.price;
    data['discount'] = this.discount;
    data['fav'] = this.fav;
    data['currency'] = this.currency;
    data['percentage'] = this.percentage;
    data['id'] = this.id;
    data['days'] = this.days;
    return data;
  }
}

class ShopsListBean {
  String image;
  String name;
  String country;
  String city;
  String category;
  String follow;
  String fav;
  int id;
  int rate;

  ShopsListBean({this.image, this.name, this.country, this.city, this.category, this.follow, this.fav, this.id, this.rate});

  ShopsListBean.fromJson(Map<String, dynamic> json) {
    this.image = json['image'];
    this.name = json['name'];
    this.country = json['country'];
    this.city = json['city'];
    this.category = json['category'];
    this.follow = json['follow'];
    this.fav = json['fav'];
    this.id = json['id'];
    this.rate = json['rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['name'] = this.name;
    data['country'] = this.country;
    data['city'] = this.city;
    data['category'] = this.category;
    data['follow'] = this.follow;
    data['fav'] = this.fav;
    data['id'] = this.id;
    data['rate'] = this.rate;
    return data;
  }
}

class SliderListBean {
  String image;
  String type;

  SliderListBean({this.image, this.type});

  SliderListBean.fromJson(Map<String, dynamic> json) {
    this.image = json['image'];
    this.type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['type'] = this.type;
    return data;
  }
}
