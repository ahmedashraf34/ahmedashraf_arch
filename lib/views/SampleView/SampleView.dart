import 'package:ahmedashraf_arch/base/BaseView.dart';
import 'package:ahmedashraf_arch/helper/CustomOverlayRoute.dart';
import 'package:ahmedashraf_arch/local/AppLocalizations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:floatingpanel/floatingpanel.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'SampleViewModel.dart';

class SampleView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SampleViewModel>(
      onModelReady: (model) {
        model.onInItSlider(context);
        model.getSample(context, isRefresh: true);
      },
      onModelDispose: (model) {
        model.pageViewController.dispose();
      },
      builder: (ctx, model, child) {
        return Scaffold(
          backgroundColor: model.colors.mainColor2,
          body: Stack(
            fit: StackFit.expand,
            children: [
              Column(
                children: [
                  build_app_bar(model, context),
                  Expanded(
                    child: model.loadingSample
                        ? model.buildCircleProgress()
                        : model.errorSample
                            ? model.buildErrorView(
                                context,
                                () {
                                  model.getSample(context, isRefresh: true);
                                },
                              )
                            : RefreshIndicator(
                                onRefresh: () {
                                  return model.getSample(context,
                                      isRefresh: true);
                                },
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      build_slider(model),
                                      build_dots(model),
                                      build_shop_list(model, context)
                                    ],
                                  ),
                                ),
                              ),
                  ),
                ],
              ),
              FloatBoxPanel(
                size: 60,
                onPressed: (index) {
                  model.onFloatButtonSelect(index);
                },

                positionTop: model.onScreenHeight(context,multiplyBy: 0.50),
                positionLeft: 0.0,
                panelShape: PanelShape.rounded,
                backgroundColor: Colors.orange,
                panelIcon: Icons.add,
                buttons: [
                  Icons.language,
                  Icons.brightness_2,
                  Icons.search,
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget build_shop_list(SampleViewModel model, BuildContext context) {
    return model.sampleModel.data.shops.isEmpty
        ? Container(
            child: model.buildEmptyView(context),
            height: model.onScreenHeight(context, multiplyBy: 0.50),
          )
        : ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            itemCount: model.sampleModel.data.shops.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: model.colors.mainColor2,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 4,
                        spreadRadius: 2,
                      )
                    ],
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            CustomOverlayRoute(
                              page: model.fullPicture(context,
                                  model.sampleModel.data.shops[index].image),
                            ),
                          );
                        },
                        child: ClipRRect(
                          child: Padding(
                            padding: EdgeInsets.all(15.0),
                            child: CachedNetworkImage(
                              imageUrl:
                                  model.sampleModel.data.shops[index].image,
                              height: 120,
                              width: 120,
                              fit: BoxFit.fill,
                              placeholder: (context, url) =>
                                  model.buildCircleProgress(),
                              errorWidget: (context, url, error) => Center(
                                child: Icon(
                                  Icons.error,
                                  color: model.colors.mainColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              model.sampleModel.data.shops[index].name,
                              style: TextStyle(
                                color: model.colors.mainColor,
                                fontFamily: model.assets.fontFamily,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text(
                              model.sampleModel.data.shops[index].category,
                              style: TextStyle(
                                color: model.colors.mainColor,
                                fontFamily: model.assets.fontFamily,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }

  Widget build_app_bar(SampleViewModel model, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 35, bottom: 15),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {},
            child: Image.asset(
              model.assets.filter,
              color: model.colors.mainColor,
              width: 30,
              height: 30,
            ),
          ),
          Spacer(),
          Text(
            AppLocalizations.of(context).home,
            style: TextStyle(
                fontFamily: model.assets.fontFamily,
                color: model.colors.mainColor,
                fontSize: 20),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {},
            child: Stack(
              children: <Widget>[
                Image.asset(
                  model.assets.notification,
                  color: model.colors.mainColor,
                  width: 30,
                  height: 30,
                ),
                Positioned(
                  top: -6.0,
                  right: 0.5,
                  child: Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1),
                        color: Colors.orange,
                        shape: BoxShape.circle),
                    child: Text(
                      '1',
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {},
            child: Stack(
              children: <Widget>[
                Image.asset(
                  model.assets.cart,
                  color: model.colors.mainColor,
                  width: 30,
                  height: 30,
                ),
                Positioned(
                  top: -6.0,
                  right: 0.5,
                  child: Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1),
                        color: Colors.orange,
                        shape: BoxShape.circle),
                    child: Text(
                      '1',
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }

  Widget build_slider(SampleViewModel model) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Container(
        height: 200,
        child: PageView.builder(
          itemCount: model.sampleModel.data?.slider?.length ?? 0,
          controller: model.pageViewController,
          itemBuilder: (BuildContext context, int index) {
            return CachedNetworkImage(
              imageUrl: model.sampleModel.data.slider[index].image,
              fit: BoxFit.fill,
              placeholder: (context, url) => model.buildCircleProgress(),
              errorWidget: (context, url, error) => Center(
                child: Icon(
                  Icons.error,
                  color: model.colors.mainColor,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget build_dots(SampleViewModel model) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: SmoothPageIndicator(
        controller: model.pageViewController,
        count: model.sampleModel.data?.slider?.length ?? 0,
        effect: ExpandingDotsEffect(
          activeDotColor: model.colors.mainColor,
          dotColor: Colors.grey,
          expansionFactor: 3,
          dotWidth: 13,
          dotHeight: 7,
        ),
      ),
    );
  }
}
