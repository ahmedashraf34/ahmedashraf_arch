import 'package:ahmedashraf_arch/base/BaseViewModel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'SampleDialog.dart';
import 'SampleModel.dart';

class SampleViewModel extends BaseViewModel {
  var newContext;

  var pageViewController = PageController();
  onInItSlider(BuildContext context) {
    newContext = context;
    Future.delayed(Duration(seconds: 2)).then((_) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => animateSlider(),
      );
    });
  }

  void animateSlider() {
    Future.delayed(
      Duration(
        seconds: 2,
      ),
    ).then((_) async {
      var nextPage = pageViewController.page.round() + 1;
      if (nextPage == sampleModel.data?.slider?.length) {
        nextPage = 0;
      }

      pageViewController
          .animateToPage(nextPage,
              duration: Duration(milliseconds: 500), curve: Curves.easeIn)
          .then((_) => animateSlider());
    });
  }


  onFloatButtonSelect(int index) {
    switch (index) {
      case 0:
        {
          return changeLanguage();
        }
        break;
      case 1:
        {
          return onChangeTheame();
        }
        break;
      case 2:
        {
          return onOpenSearch();
        }
        break;
    }
  }
  onChangeTheame() {
    pref.isDarkMode = !pref.isDarkMode;
    if (pref.isDarkMode) {}
    notifyListeners();
  }


  var newSearchWord = '';

  onOpenSearch() {
    return showDialog(
      context: newContext,
      builder: (BuildContext context) {
        return SampleDialog();
      },
    ).then((value) {
      newSearchWord = value;
      if (newSearchWord != '' || newSearchWord != null) {
        getSample(newContext, isRefresh: false, search: false);
      }
    });
  }

  final searchController = TextEditingController();
  var searchWord;
  var usingSearch = true;

  onClearSearchWord() {
    searchWord = null;
    searchController.text = "";
    notifyListeners();
  }

  onChangeSearchWord(String word) {
    searchWord = word;
    notifyListeners();
  }

  bool loadingSample = false;
  bool errorSample = false;
  SampleModel sampleModel;

  Future<void> getSample(BuildContext context,
      {bool isRefresh = false, bool search = true}) async {
    if (isRefresh) {
      loadingSample = true;
      notifyListeners();
    }
    if (search) {
      usingSearch = true;
    }
    try {
      Response response = await api
          .onSample(
        search: newSearchWord,
      )
          .timeout(Duration(seconds: 30), onTimeout: () {
        errorSample = true;
        loadingSample = false;
        usingSearch = false;
        notifyListeners();
        return;
      });
      sampleModel = SampleModel.fromJson(response.data);
      if (response.data["key"] != "success") {
        errorSample = true;
        loadingSample = false;
        notifyListeners();
      }
      notifyListeners();
    } on DioError catch (e) {
      getDioError(context, error: errorSample, e: e, loading: loadingSample);
    }
    loadingSample = false;
    errorSample = false;
    notifyListeners();
    return;
  }
}
