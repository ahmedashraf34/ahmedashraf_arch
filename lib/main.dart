import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'base/LifeCycleManager.dart';
import 'base/Locator.dart';
import 'base/Router.dart';
import 'local/AppLocalizations.dart';
import 'services/ColorsService.dart';
import 'services/LocalizationService.dart';
import 'services/NavigationService.dart';
import 'services/PrefService.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  return runApp(
    MyApp(),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var localService = locator<LocalizationService>();
  final pref = locator<PrefService>();
  final colors = locator<ColorsService>();

  @override
  void initState() {
    super.initState();
    localService.onLocaleChanged = onLocaleChange;
  }

  onLocaleChange(String countryCode) {
    setState(() {
      localService.changeLocal(countryCode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return LifeCycleManager(
      child: MaterialApp(
        builder: BotToastInit(),
        title: 'ahmedashraf arch',
        theme: ThemeData(
          cursorColor: colors.darkblueColor,
          primarySwatch: Colors.indigo,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'bahjii',
        ),
        debugShowCheckedModeBanner: false,
        navigatorObservers: [BotToastNavigatorObserver()],
        onGenerateRoute: Router.generateRoute,
        navigatorKey: locator<NavigationService>().navigatorKey,
        supportedLocales: [
          Locale("en"),
          Locale("ar"),
        ],
        locale: localService.currentLocal.overriddenLocale,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          FallbackCupertinoLocalisationsDelegate(),
          localService.currentLocal
        ],
      ),
    );
  }
}
