import 'package:ahmedashraf_arch/services/PrefService.dart';
import 'package:ahmedashraf_arch/views/SampleView/SampleView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Locator.dart';
import 'dart:io';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return Platform.isIOS
            ? CupertinoPageRoute(
                builder: (_) => SampleView(),
              )
            : MaterialPageRoute(
                builder: (_) => SampleView(),
              );

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
