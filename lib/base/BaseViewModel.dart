import 'package:ahmedashraf_arch/local/AppLocalizations.dart';
import 'package:ahmedashraf_arch/services/ApiService.dart';
import 'package:ahmedashraf_arch/services/AssetsService.dart';
import 'package:ahmedashraf_arch/services/ColorsService.dart';
import 'package:ahmedashraf_arch/services/LocalizationService.dart';
import 'package:ahmedashraf_arch/services/NavigationService.dart';
import 'package:ahmedashraf_arch/services/PrefService.dart';
import 'package:ahmedashraf_arch/services/StatusBarService.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';

import 'Locator.dart';

class BaseViewModel extends ChangeNotifier {
  final pref = locator<PrefService>();
  final statusBar = locator<StatusBarService>();
  final api = locator<ApiService>();
  final colors = locator<ColorsService>();
  final assets = locator<AssetsService>();
  final nav = locator<NavigationService>();
  final localService = locator<LocalizationService>();

  double onScreenHeight(BuildContext context, {double multiplyBy = 1}) {
    return MediaQuery.of(context).size.height * multiplyBy;
  }

  double onScreenWidth(BuildContext context, {double dividedBy = 1}) {
    return MediaQuery.of(context).size.width / dividedBy;
  }

  Center buildCircleProgress() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  onShowCustomeDialog({String msg, bool isSuccess, bool isLoading}) {
    return BotToast.showCustomLoading(
      allowClick: true,
      animationDuration: Duration(milliseconds: 500),
      animationReverseDuration: Duration(milliseconds: 500),
      clickClose: true,
      onClose: () {
//        FocusScope.of(context).unfocus();
      },
      toastBuilder: (void Function() cancelFunc) {
        return Center(
          child: Container(
            width: 300,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
            ),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  isLoading
                      ? Center(child: CircularProgressIndicator())
                      : Icon(
                          isSuccess ? Icons.check_circle : Icons.error,
                          size: 60,
                          color: isSuccess ? Colors.green : Colors.red,
                        ),
                  Text(
                    msg,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  onShowBotToast(String text, IconData icon, Color color) {
    return BotToast.showNotification(
      enableSlideOff: true,
      trailing: (cancel) {
        return GestureDetector(onTap: cancel, child: Icon(Icons.clear));
      },
      onlyOne: true,
      title: (_) => Text(
        "${text}",
        style: TextStyle(fontFamily: assets.fontFamily, shadows: [
          BoxShadow(color: Colors.white),
        ]),
        textDirection: TextDirection.rtl,
      ),
      crossPage: true,
      leading: (_) => Icon(
        icon,
        color: color,
        size: 35,
      ),
      animationDuration: Duration(milliseconds: 500),
      animationReverseDuration: Duration(milliseconds: 500),
      duration: Duration(seconds: 3),
    );
  }

  Widget buildErrorView(BuildContext context,Function reload) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context).networkCheck,
            style: TextStyle(
              color: colors.mainColor,
              fontWeight: FontWeight.bold,
              fontFamily: assets.fontFamily,
              fontSize: 25,
            ),
          ),
          RaisedButton(
            onPressed: (){
              reload();
            },
            child: Text('حاول مجداا'),
          )
        ],
      ),
    );
  }

  Widget buildEmptyView(BuildContext context) {
    return Center(
      child: Text(
        "لا توجد منتجات",
        style: TextStyle(
          color: colors.mainColor,
          fontWeight: FontWeight.bold,
          fontFamily: assets.fontFamily,
          fontSize: 25,
        ),
      ),
    );
  }

  onChangeLang(String countryCode) {
    localService.onLocaleChanged(countryCode);
    notifyListeners();
  }

  changeLanguage() {
    if (pref.userLocal == "en") {
      onChangeLang("ar");
      notifyListeners();
    } else {
      onChangeLang("en");
      notifyListeners();
    }
  }

  fullPicture(BuildContext context, String image) {
    return Dismissible(
      direction: DismissDirection.vertical,
      key: Key(image),
      onDismissed: (DismissDirection direction) {
        Navigator.of(context).pop();
      },
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.8),
            ),
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30, right: 10),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: colors.mainColor,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: PhotoView(
              imageProvider: CachedNetworkImageProvider(image),
              backgroundDecoration:
                  BoxDecoration(color: Colors.black.withOpacity(0.8)),
              enableRotation: false,
              minScale: PhotoViewComputedScale.contained * 0.8,
            ),
          ),
          Container(
            color: Colors.black.withOpacity(0.8),
            height: 100,
          ),
        ],
      ),
    );
  }

  getDioError(BuildContext context, {var e, bool loading, bool error}) {
    if (DioErrorType.RECEIVE_TIMEOUT == e.type ||
        DioErrorType.CONNECT_TIMEOUT == e.type) {
      print('case 1');
      print("Server is not reachable. Please verify your internet connection and try again");
    } else if (DioErrorType.RESPONSE == e.type) {
      print('case 2');
      print("Server reachable. Error in resposne");
      error = true;
      loading = false;
      onShowBotToast(e.response.data.toString(), Icons.error, Colors.red);
      notifyListeners();
    } else if (DioErrorType.DEFAULT == e.type) {
      if (e.message.contains('SocketException')) {
        print("Network error");
        print('case 3');
        onShowBotToast(AppLocalizations.of(context).networkCheck, Icons.error, Colors.red);
        error = true;
        loading = false;
        notifyListeners();
      }
    } else {
      print('case 4');
      print("Problem connecting to the server. Please try again.");
    }
  }
}
