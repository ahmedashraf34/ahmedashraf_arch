import 'package:ahmedashraf_arch/services/ApiService.dart';
import 'package:ahmedashraf_arch/services/AssetsService.dart';
import 'package:ahmedashraf_arch/services/ColorsService.dart';
import 'package:ahmedashraf_arch/services/LocalizationService.dart';
import 'package:ahmedashraf_arch/services/NavigationService.dart';
import 'package:ahmedashraf_arch/services/PrefService.dart';
import 'package:ahmedashraf_arch/services/StatusBarService.dart';
import 'package:ahmedashraf_arch/views/SampleView/SampleViewModel.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

GetIt locator = GetIt.instance;

void setupLocator() async {
  var prefInstance = await PrefService.getInstance();
  locator.registerSingleton<PrefService>(prefInstance);
  locator.registerLazySingleton(() => StatusBarService());
  locator.registerLazySingleton(() => ApiService());
  locator.registerLazySingleton(() => ColorsService());
  locator.registerLazySingleton(() => AssetsService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => LocalizationService());

  //Views
  locator.registerFactory(() => SampleViewModel());


  locator.registerLazySingleton(() => PublishSubject<bool>());
  locator.registerLazySingleton(() => PublishSubject<String>());
}
