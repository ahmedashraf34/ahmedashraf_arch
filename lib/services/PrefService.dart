import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefService {
  static SharedPreferences _preferences;
  static PrefService _instance;

  static Future<PrefService> getInstance() async {
    if (_instance == null) {
      _instance = PrefService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  void setUserData({
    @required String token,
    @required Map<String, dynamic> data,
  }) {
    _preferences.setString('userData', json.encode(data));
  }

  Map getUserData() {
    final dataStr = _preferences.getString('userData');
    if (dataStr == null) return null;
    return json.decode(dataStr);
  }


  bool get isDarkMode => _preferences.getBool("isDarkMode") ?? false;
  set isDarkMode(bool value) => _preferences.setBool("isDarkMode", value);


  String get userLocal => _preferences.getString("userLocal") ?? "ar";
  set userLocal(String value) => _preferences.setString("userLocal", value);




}
