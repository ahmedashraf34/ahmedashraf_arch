import 'package:ahmedashraf_arch/base/Locator.dart';

import 'PrefService.dart';

class AssetsService {
  var pref = locator<PrefService>();
  var base = "assets/icons/";
  var baseExt = ".png";



  String get fontFamily => 'bahjii';
  String get viktor => base + "viktor" + baseExt;
  String get filter => base + "filter" + baseExt;
  String get notification => base + "notification" + baseExt;
  String get cart => base + "cart" + baseExt;

}
