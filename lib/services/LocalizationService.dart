import 'dart:ui';
import 'package:ahmedashraf_arch/local/AppLocalizations.dart';
import 'package:flutter/material.dart';
import 'package:ahmedashraf_arch/base/Locator.dart';
import 'PrefService.dart';

typedef void LocaleChangeCallback(String countryCode);

class LocalizationService {
  LocaleChangeCallback onLocaleChanged;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  var pref = locator<PrefService>();

  LocalizationService() {
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(Locale(pref.userLocal));
  }

  SpecificLocalizationDelegate get currentLocal =>
      _specificLocalizationDelegate;

  void changeLocal(String countryCode) {
    pref.userLocal = countryCode;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(Locale(countryCode));
  }
}
