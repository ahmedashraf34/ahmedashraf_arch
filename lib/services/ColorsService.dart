import 'package:ahmedashraf_arch/base/Locator.dart';
import 'package:flutter/material.dart';
import 'PrefService.dart';

class ColorsService {
  var pref = locator<PrefService>();

  final blackColor = Color(0xff0B0F35);
  final whiteColor = Color(0xFFFFFFFF);
  final greyColor = Color(0xff707070);
  final blueColor = Color(0xff3398DB);
  final semiGreyColor = Color(0xffECF0F1);
  final semiBoldGreyColor = Color(0xffB2B2B2);
  final hardGreyColor = Color(0xff606060);
  final darkblueColor = Color(0xff141A46);
  final orangeColor = Color(0xffFBA100);
  final redColor = Color(0xffFF0000);
  final semiOrangeColor = Color(0xffEB8A5E);
  final yellowColor = Color(0xffFBDA00);




  Color get mainColor {
    if (pref.isDarkMode != true) {
      return blackColor;
    } else {
      return whiteColor;
    }
  }

  Color get mainColor2 {
    if (pref.isDarkMode == true) {
      return blackColor;
    } else {
      return whiteColor;
    }
  }
}
