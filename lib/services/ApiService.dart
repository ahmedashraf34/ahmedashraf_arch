import 'package:ahmedashraf_arch/base/Locator.dart';
import 'package:ahmedashraf_arch/helper/DioConnectivityRequestRetrier.dart';
import 'package:ahmedashraf_arch/helper/RetryOnConnectionChangeInterceptor.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'PrefService.dart';

class ApiService {
  Dio _dio;
  static const base = "https://app.2day-offers.com/api/";
  var _pref = locator<PrefService>();

  ApiService() {
    _dio = Dio()
      ..interceptors.add(
        LogInterceptor(
          responseBody: true,
          error: true,
          requestBody: true,
          requestHeader: true,
        ),
      )
      ..interceptors.add(
        RetryOnConnectionChangeInterceptor(
          requestRetrier: DioConnectivityRequestRetrier(
            connectivity: Connectivity(),
            dio: Dio(),
          ),
        ),
      );
  }





  Future<Response> onSample({
    String search,
  }) {
    Map<String, String> header = {};
    Map<String, dynamic> body = {
      "lang": _pref.userLocal,
    };
    if (search != null) {
      body["search"] = search;
    }

    return _dio.post(
      base + "home",
      options: Options(headers: header),
      data: body,
    );
  }
}
